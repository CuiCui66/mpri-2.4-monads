(* Structure *)

include Monad.FullMonad

(* Operations *)

val fail : unit -> 'a t
val either : 'a t -> 'a t -> 'a t

(* Runners *)

val run : 'a t -> 'a
val all : 'a t -> 'a Seq.t

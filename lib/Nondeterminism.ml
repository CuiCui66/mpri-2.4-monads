(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Base = struct
  type 'a t = 'a list

  let return a = [a]

  let bind m f = List.concat_map f m

end

module M = Monad.Expand (Base)
include M

let fail () = []

let either = List.append

exception NondeterminismFail

let run m = match m with
  | [] -> raise NondeterminismFail
  | a::t -> a

let all = List.to_seq

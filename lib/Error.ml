module Base = struct
  type 'a t =
    | Val of 'a
    | Err of exn

  let return a = Val a

  let bind m f = match m with
    | Val a -> f a
    | Err e -> Err e

end

module M = Monad.Expand (Base)
include M
open Base

let err e = Err e

let try_with_finally m ks kf = match m with
  | Val a -> ks a
  | Err e -> kf e

let run = function
  | Val a -> a
  | Err e -> raise e

(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-33-37-39"]

open Monads

(* Taken from [https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1] *)

(* Passes a string of dictionary {a,b,c}
 * Game is to produce a number from the string.
 * By default the game is off, a 'c' toggles the
 * game on and off.
 * A 'a' gives +1 and a 'b' gives -1 when the game is on,
 * nothing otherwise.
 *)

module U = Monads.State.Make(struct type t = unit let init = () end)

let playGame s =
  let rec playGameFrom i on acc =
    if i == String.length s then U.return acc
    else
      match s.[i] with
      | 'a' when on -> playGameFrom (i + 1) on (acc + 1)
      | 'b' when on -> playGameFrom (i + 1) on (acc - 1)
      | 'c' -> playGameFrom (i + 1) (not on) acc
      | _ -> playGameFrom (i + 1) on acc
  in
  playGameFrom 0 false 0

let result s = U.run (playGame s)

let%test _ = result "ab" = 0
let%test _ = result "ca" = 1
let%test _ = result "cabca" = 0
let%test _ = result "abcaaacbbcabbab" = 2

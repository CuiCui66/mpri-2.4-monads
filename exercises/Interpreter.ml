(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-37-39"]

type value =
  | IsNat of int
  | IsBool of bool

type exp =
  | Val of value
  | Eq of exp * exp
  | Plus of exp * exp
  | Ifte of exp * exp * exp

open Monads.Error

exception RunTimeError of string

let rec sem = function
  | Val v -> return v
  | Eq (e, e') ->
    begin
      let* a = sem e and* b = sem e' in
      match a, b with
      | IsNat i, IsNat i2 -> return @@ IsBool (i = i2)
      | IsBool b, IsBool b2 -> return @@ IsBool (b = b2)
      | _ -> err @@ RunTimeError "type mismatch in equality"
    end
  | Plus (e, e') ->
    begin
      let* a = sem e and* b = sem e' in
      match a, b with
      | IsNat i, IsNat i2 -> return @@ IsNat (i + i2)
      | _ -> err @@ RunTimeError "type mismatch in addition"
    end
  | Ifte (e,e',e'') ->
    begin
      let* c = sem e in
      match c with
      | IsNat _ -> err @@ RunTimeError "if condition is an int"
      | IsBool true -> sem e'
      | IsBool false -> sem e''
    end

(** * Tests *)

let%test _ = run (sem (Val (IsNat 42))) = IsNat 42

let%test _ = run (sem (Eq (Val (IsBool true), Val (IsBool true)))) = IsBool true

let%test _ = run (sem (Eq (Val (IsNat 3), Val (IsNat 3)))) = IsBool true

let%test _ =
  run (sem (Eq (Val (IsBool true), Val (IsBool false)))) = IsBool false

let%test _ = run (sem (Eq (Val (IsNat 42), Val (IsNat 3)))) = IsBool false

let%test _ = run (sem (Plus (Val (IsNat 42), Val (IsNat 3)))) = IsNat 45

let%test _ =
  run (sem (Ifte (Val (IsBool true), Val (IsNat 42), Val (IsNat 3)))) = IsNat 42

let%test _ =
  run (sem (Ifte (Val (IsBool false), Val (IsNat 42), Val (IsNat 3)))) = IsNat 3

let%test _ =
  run
    (sem
       (Ifte
          ( Eq (Val (IsNat 21), Plus (Val (IsNat 20), Val (IsNat 1)))
          , Val (IsNat 42)
          , Val (IsNat 3) )))
  = IsNat 42

(** ** Ill-typed expressions *)

let%test _ =
  try
    ignore(run (sem (Plus (Val (IsBool true), Val (IsNat 3)))));
    false
  with
    _ -> true

let%test _ =
  try
    ignore (run (sem (Ifte (Val (IsNat 3), Val (IsNat 42), Val (IsNat 44)))));
    false
  with
    _ -> true
